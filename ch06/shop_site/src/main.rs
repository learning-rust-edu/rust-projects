use actix_files as fs;
use actix_web::{get, web, App, HttpResponse, HttpServer, Responder, HttpRequest};
use shop_base::{Conn};

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

async fn find_items(req: HttpRequest) -> impl Responder {
    let st= req.match_info().query("search_term").parse().unwrap_or("".to_string());
    let conn = Conn::new().unwrap();
    let items = conn.find_items(&st, 5).unwrap();
    HttpResponse::Ok().json(items)
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(move || {
        App::new()
            .service(
                web::scope("/db")
                    .route(
                        "/",
                        web::get().to(|| {
                            HttpResponse::Ok()
                                .content_type("text/plain")
                                .body("This is the Database side of the app")
                        }),
                    )
                    .route("/find_items", web::get().to(find_items)),
            )
            .service(
                fs::Files::new("/", "test_site/static/")
                    .show_files_listing()
                    .index_file("index.html"),
            )
    })
    .bind("127.0.0.1:8000")?
    .run()
    .await
}
