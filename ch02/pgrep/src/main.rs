use clap::{crate_version, App, Arg};
use failure::{Error, Fail};
use regex::Regex;
use std::path::Path;

#[derive(Debug, Fail)]
#[fail(display = "Argument not provided '{}'", arg)]
struct ArgErr {
    arg: &'static str,
}

#[derive(Debug)]
struct Record {
    line: usize,
    text: String,
}

fn process_file<P: AsRef<Path>>(p: P, re: &Regex) -> Result<Vec<Record>, Error> {
    let mut res = Vec::new();
    let bts = std::fs::read(p)?;
    if let Ok(ss) = String::from_utf8(bts) {
        for (index, line) in ss.lines().enumerate() {
            if re.is_match(line) {
                res.push(Record {
                    line: index,
                    text: line.to_string(),
                });
            }
        }
    }
    Ok(res)
}

fn process_path<P, FF, EF>(p: P, re: &Regex, ff: &FF, ef: &EF) -> Result<(), Error>
where
    P: AsRef<Path>,
    FF: Fn(&Path, Vec<Record>),
    EF: Fn(Error),
{
    let p = p.as_ref();
    let md = p.metadata()?;
    let ft = md.file_type();
    if ft.is_file() {
        let data = process_file(p, re)?;
        ff(p, data);
    }

    if ft.is_dir() {
        let dir = std::fs::read_dir(p)?;
        for d in dir {
            if let Err(e) = process_path(d?.path(), re, ff, ef) {
                ef(e);
            }
        }
    }
    Ok(())
}

fn main() {
    if let Err(e) = run() {
        println!("There was an error: {}", e);
    }
}

fn run() -> Result<(), Error> {
    let cp = App::new("pgrep")
        .version(crate_version!())
        .about("A grep like program")
        .author("Edu")
        .arg(
            Arg::with_name("file")
                .short("f")
                .long("file")
                .takes_value(true)
                .help("The file to test"),
        )
        .arg(
            Arg::with_name("pattern")
                .required(true)
                .help("The pattern to search for"),
        )
        .get_matches();

    let re = Regex::new(cp.value_of("pattern").unwrap())?;
    let p = process_path(
        cp.value_of("file").ok_or(ArgErr { arg: "file" })?,
        &re,
        &|pt, v| {
            println!("{:?}", pt);
            println!("{:?}", v);
        },
        &|e| {
            println!("Error:{}", e);
        },
    );
    println!("{:?}", p);
    Ok(())
}
