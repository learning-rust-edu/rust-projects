use futures::future::Future;
use futures::task::{Context, Poll};
use std::pin::Pin;

pub struct ChatBox {}

impl Future for ChatBox {
    type Output = String;

    fn poll(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Self::Output> {
        Poll::Ready("hello".to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use futures::FutureExt;

    #[tokio::test]
    async fn it_works() {
        let f = ChatBox {};
        let p = f.map(|s| println!("{}", s));
        println!("Beginning....");
        let handle = tokio::task::spawn(p);
        println!("End");
        handle.await.unwrap();
    }
}
